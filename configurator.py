"""Configurator Module."""
import configparser
import logging
import logging.config


class Configurator(object):
    """Configurator Class definition."""

    def __init__(self, fpath='config.ini'):
        """Initialize Configurator class."""
        self.logger = logging.getLogger(__name__)

        self.logger.info('Initializing Configurator')

        self.fpath = fpath
        self.parse_config()
        self.set_sections()
        self.set_settings()

        self.logger.info('Configurator initialized')
        self.logger.debug(self)

    def parse_config(self):
        """Parse configuration file."""
        Config = configparser.ConfigParser()
        Config.read(self.fpath)

        self.logger.info('Parse the config file %s' % self.fpath)

        self.config = Config

    def set_sections(self):
        """Map Configuration Section Options."""
        sections = self.config.sections()

        self.logger.info('Sections retrieved')
        self.logger.debug(sections)

        self.sections = sections

    def set_settings(self):
        """Set settings."""
        settings = dict()
        for section in self.sections:
            settings[section] = dict()
            options = self.config.options(section)

            self.logger.info('Options retrieved')
            self.logger.debug('Options: %s' % options)

            for option in options:
                try:
                    settings[section][option] = self.config.get(section,
                                                                option)
                    self.logger.debug("Settings from %s in %s" %
                                      (option, section))

                except Exception:
                    logging.exception("Exception on %s" % option)
                    settings[section][option] = None
        self.settings = settings


def main():
    """Test the Configurator class definition."""
    logging.config.fileConfig('logging.ini')
    Configurator()


if __name__ == '__main__':
    main()
