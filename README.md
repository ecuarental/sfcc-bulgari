# SFCC Extractor Tool

-   SFCC Extractor Tool optimized for Bulgari
-   Bitbucket repository: [bitbucket.org/ecuarental/sfcc-bulgari](https://bitbucket.org/ecuarental/sfcc-bulgari)

## Environment setup

### Install python 3

*   For Mac OSX: [docs.python-guide.org/en/latest/starting/install3/osx/](http://docs.python-guide.org/en/latest/starting/install3/osx/)
*   For Windows: [docs.python-guide.org/en/latest/starting/install3/win/#install3-windows](http://docs.python-guide.org/en/latest/starting/install3/win/#install3-windows)

### Setup the virtualenv

A Python Virtual Environment gives us the possibility to encapsulate a working environment with the proper python interpreter and the libraries required to run the application

**For Mac OSX:**

1.  Create the virtualenv
        ```shell
        $ virtualenv -p python3 venv
        ```
        > venv/ will be the folder where the python3 interpreter will be installed with the required libraries

1.  Activate the virtualenv
        ```shell
        $ source venv/bin/activate
        ```

1.  Install the libraries required from the requirements.txt file
        ```shell
        $ pip install -r requirements.txt
        ```

**For Windows Powershell:**

1.  Create the virtualenv
        ```shell
        $ virtualenv venv
        ```
        > for Windows Powershell there is no need to specify the python3 interpreter

1.  Activate the virtualenv
        ```shell
        $ . venv/lib/activate-ps
        ```

1.  Install the libraries required from the requirements.txt file
        ```shell
        $ pip install -r requirements.txt
        ```

## Bulgari Details

1.  Bulgari US Store [www.bulgari.com/en-us](https://www.bulgari.com/en-us/)

## Useful documentation

*   HTML Parser: BeautifulSoup4 - [www.crummy.com/software/BeautifulSoup/bs4/doc](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
*   Template engine: Jinja2 - [jinja.pocoo.org/docs/2.10/](http://jinja.pocoo.org/docs/2.10/)
*   Python Guide: The Hitchhiker’s Guide to Python! - [docs.python-guide.org/en/latest/](http://docs.python-guide.org/en/latest/)
*   Webdriver Selenium: [selenium-python.readthedocs.io/api.html](http://selenium-python.readthedocs.io/api.html)
