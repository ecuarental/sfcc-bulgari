"""Parser module."""
from website import WebPage
from product import ProductGrid
from model import JSONHandler
import os
from utils import read_file


class Parser(object):
    """Parser Class definition."""

    def __init__(self, fpath, category_id=None):
        """Initialize Parser Class."""
        super(Parser, self).__init__()
        self.fpath = fpath
        self.category_id = category_id
        self.output_basepath = "output/json/"
        self.set_output_path()

    def set_output_path(self):
        """Set the outpath."""
        filename = 'products-{}.json'.format(self.category_id)
        output_path = os.path.join(self.output_basepath, filename)
        self.output_path = output_path

    def execute(self):
        """Execute the parser."""
        src = read_file(self.fpath)
        webpage = WebPage(src, category_id=self.category_id)
        prod_grid = ProductGrid(webpage.bs, category_id=self.category_id)

        json_handler = JSONHandler(self.output_path, prod_grid.products)
        json_handler.write_obj_dict_list_to_json()
