"""Templater module."""
from utils import render_template, read_json
from os.path import join

PROSPECT_LONG = 'Bulgary'
PROSPECT_SHORT = 'blg'
OUTPUT_BPATH = 'output/xml/'
TEMPLATE_BPATH = 'source/templates/'
INPUT_BPATH = 'output/json/'


class Templater(object):
    """Templater Class definition."""

    def __init__(self, template, outfile):
        """Initialize Templater."""
        super(Templater).__init__()
        self.template_path = join(TEMPLATE_BPATH, template)
        self.context = dict(Context())
        self.outfile = join(OUTPUT_BPATH, outfile)

    def execute(self):
        """Render the context using the template."""
        with open(self.outfile, 'w') as outfile:
            outfile.write(render_template(self.template_path, self.context))


class Context(object):
    """Context Class definition."""

    def __init__(self):
        """Initialize Context."""
        self.prospect_long = PROSPECT_LONG
        self.prospect_short = PROSPECT_SHORT
        self.set_products()
        self.set_categories()
        self.set_prices()

    def set_products(self):
        """Set products."""
        products_path = join(INPUT_BPATH, 'products.json')
        self.products = read_json(products_path)

    def set_categories(self):
        """Set categories."""
        categories_path = join(INPUT_BPATH, 'categories.json')
        self.categories = read_json(categories_path)

    def set_prices(self):
        """Set prices."""
        prices_path = join(INPUT_BPATH, 'prices.json')
        self.prices = read_json(prices_path)

    def __iter__(self):
        """Iterate over some attributes."""
        yield ('products', self.products)
        yield ('categories', self.categories)
        yield ('prices', self.prices)
