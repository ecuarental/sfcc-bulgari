"""Model module."""
import os
import json
import logging
from utils import read_json, write_json
from os.path import join

OUTPUT_BPATH = 'output/json/'
INPUT_BPATH = 'source/json/'


class JSONHandler(object):
    """JSONHandler Class definition."""

    def __init__(self, output_path, obj_list):
        """Initialize JSONHandler."""
        super(JSONHandler, self).__init__()
        self.logger = logging.getLogger(__name__)
        self.logger.info('Initializing JSONHandler')
        self.output_path = output_path
        self.obj_list = obj_list
        self.set_obj_dict_list()
        self.logger.info('JSONHandler initialized')
        self.logger.info(self)

    def set_obj_dict_list(self):
        """Set the object dictionnary."""
        obj_dict_list = list()
        self.logger.info('Populate the list of object dicts')

        for obj_item in self.obj_list:
            obj_dict_list.append(dict(obj_item))
            self.logger.debug('Appended {}'.format(obj_item))
        self.logger.info('Successfuly populated the obj_dict_list')
        self.logger.info('Populated with {}# objects'
                         .format(len(self.obj_list)))

        self.obj_dict_list = obj_dict_list

    def write_obj_dict_list_to_json(self):
        """Write dictionnary to json file."""
        if not os.path.exists(os.path.dirname(self.output_path)):
            self.logger.warn('{} does not exist'.format(self.output_path))
            os.makedirs(os.path.dirname(self.output_path))
            self.logger.info('{} created'.format(self.output_path))

        with open(self.output_path, 'w') as outfile:
            json.dump(self.obj_dict_list, outfile, indent=2)
            self.logger.info('{} successfully created'
                             .format(self.output_path))
            self.logger.debug(self.obj_dict_list)

    def __repr__(self):
        """Representate JSONHandler."""
        return '<JSONHandler: {} filepath - {}# objects>'\
            .format(self.output_path, len(self.obj_dict_list))


class ProductsBundler(object):
    """ProductsBundler."""

    def __init__(self, files):
        """Initialize JSONJoiner."""
        self.logger = logging.getLogger(__name__)
        self.files = files
        self.set_products()

    def set_products(self):
        """Join product JSON files."""
        products_list = list()

        for fname in self.files:
            fpath = join(OUTPUT_BPATH, fname)
            products = read_json(fpath)

            for product in products:
                products_list.append(product)
        self.logger.info('{}# of products aggregated'
                         .format(len(products_list)))
        self.products = products_list

    def execute(self):
        """Write products JSON."""
        outpath = join(OUTPUT_BPATH, 'products.json')
        write_json(self.products, outpath)
