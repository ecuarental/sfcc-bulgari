"""Utilities module."""
import os
import jinja2
import json
import pprint
import hashlib
import requests
import logging
# import shutil

from os import listdir
from os.path import isfile, join

from urllib.parse import urlparse

# The BeautifulSoup module
# from bs4 import BeautifulSoup

# The selenium module
from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import _find_element


class text_to_change(object):
    '''Custom Expected Condition for Selenium Webdriver.'''

    def __init__(self, locator, text):
        self.locator = locator
        self.text = text

    def __call__(self, driver):
        actual_text = _find_element(driver, self.locator).text
        return actual_text != self.text


def render_template(tpl_path, context):
    '''
    Render the context data into the template located in tpl_path

    Args:
        tpl_path (str): Jinja2 template file path
        context (dict): data that will be rendered in with the template
            tpl_path
    '''
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)


def write_json(dict, json_file):
    '''
    Write dictionnary to json file

    Args:
        dict (dict): dictionnary variable to write
        json_file (str): json file path to write
    '''
    if not os.path.exists(os.path.dirname(json_file)):
        os.makedirs(os.path.dirname(json_file))
    with open(json_file, 'w') as outfile:
        json.dump(dict, outfile, indent=2)


def read_json(json_file):
    '''
    Read json file into a dictionnary variable

    Args:
        dict (dict): dictionnary variable to write
        json_file (str): json file path to write
    Return:
        output (dict): json data into a dictionnary variable
    '''
    with open(json_file, 'r') as infile:
        output = json.load(infile)
    return output


def pprint_ar(ar_to_print):
    pprint.pprint(ar_to_print)


def print_var(var_name, var_value):
    """Print the var_name or message plus the var_value.

    Useful for debugging

    Args:
        var_name (str): message o variable name to print
        var_value (str, int, float): to print

    """
    print(var_name, str(var_value))


def gen_uuid(name):
    """Gen UUID based on the name arg.

    Args:
        name (str): name to be hashed.

    """
    h = hashlib.md5()
    h.update(name.encode('utf-8'))
    id_out = h.hexdigest()
    id_out = id_out.upper()
    return id_out


def download_file(url, filename):
    if filename is '':
        path = 'downloads/'
        filename = url.split('/')[-1]
        filename += path

    path = os.path.dirname(filename)
    print("This is the path {}".format(path))
    # path = os.path.join(os.path.curdir, path)
    # print("This is the abs path {}".format(path))
    if not os.path.exists(path):
        os.makedirs(path)

    r = requests.get(url, stream=True)

    if not os.path.exists(path):
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
    return filename


def download_html(url, css_selector='body', cookie=None):
    """Download HTML with WebDriver.

    Keyword arguments:
        css_selector (str): css selector of the element to wait to be loaded
        cookie (str): cookie to inject

    Returns:
        str: the page source of the url

    """
    driver = webdriver.Chrome()
    driver.get(url)

    if cookie is not None:
        driver.add_cookie(cookie)
        driver.get(url)

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
        (By.CSS_SELECTOR, css_selector)
    ))

    src = driver.page_source

    driver.close()

    return src


def download_html_text_change(url, css_selector='body'):
    driver = webdriver.Chrome()
    driver.get(url)

    WebDriverWait(driver, 10).until(
        text_to_change((By.CSS_SELECTOR, css_selector), '')
    )

    src = driver.page_source

    driver.close()

    return src


def get_base_url(url):
    url_parsed = urlparse(url)
    method = url_parsed.scheme + '://'
    domain = url_parsed.netloc
    base_url = method + domain
    return base_url


def get_path_url(url):
    url_parsed = urlparse(url)
    path = url_parsed.path
    return path


def gen_category_id(name):
    output = name.lower().replace(' ', '_')
    return output


def gen_id(name):
    output = name.lower().replace(' ', '_')
    return output


def clean_html(bs_obj):
    for tag in bs_obj():
        for attr in ["style"]:
            del tag[attr]

    return bs_obj


def uniquify_list(list_var):
    output_set = set(list_var)
    output_list = list(output_set)
    return output_list


def get_files_from_dir(dir_path):
    files = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
    return files


def get_files_from_dir_relative(dir_path):
    files = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
    files_output = list()
    for f in files:
        f = join(dir_path, f)
        files_output.append(f)
    return files_output


def get_aggregated_list(json_files):
    aggregated_list = []
    for f in json_files:
        items = read_json(f)
        aggregated_list.extend(items)

    return aggregated_list


def get_country_code(country):
    countries = read_json('countries.json')
    for c in countries:
        if country.lower() == c['name'].lower():
            country_code = c['code']
            return country_code


def read_file(input_file):
    """Read file.

    Args:
        input_file (str): input file path

    Returns:
        str: file contents

    """
    if not os.path.exists(input_file):
        logging.warn('{} does not exist'.format(input_file))
        return None
    else:
        with open(input_file, 'r') as infile:
            output = infile.read()
        logging.debug(output)
        return output
