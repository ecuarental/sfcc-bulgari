"""Application Module."""
import logging
import logging.config
from parser import Parser
from templater import Templater
from model import ProductsBundler
from product import Category
from utils import write_json
from os.path import join

OUTPUT_BPATH = 'output/json/'


def parse_products():
    """Parse products."""
    parser = Parser('source/html/products-rings.html', 'JewelryRings')
    parser.execute()

    parser = Parser('source/html/products-jewelry-rings-bzero1.html',
                    'JewelryRingsBzero1')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-rings-newyork-collection.html',
        'JewelryRingsNewYork')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-rings-serpenti.html',
        'JewelryRingsSerpenti')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-rings-bulgari.html',
        'JewelryRingsBulgari')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-necklaces-bzero1.html',
        'JewelryNecklacesBzero1')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-necklaces-divas-dream.html',
        'JewelryNecklacesDivasDream')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-necklaces-serpenti.html',
        'JewelryNecklacesSerpenti')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-bracelets-bzero1.html',
        'JewelryBraceletsBzero1')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-bracelets-divas-dream.html',
        'JewelryBraceletsDivasDream')
    parser.execute()

    parser = Parser(
        'source/html/products-jewelry-bracelets-serpenti.html',
        'JewelryBraceletsSerpenti')
    parser.execute()


def join_products():
    """Join products JSON files into one products.json."""
    product_files = [
        'products-JewelryRings.json',
        'products-JewelryRingsBzero1.json',
        'products-JewelryRingsBulgari.json',
        'products-JewelryRingsNewYork.json',
        'products-JewelryRingsSerpenti.json',
        'products-JewelryNecklacesBzero1.json',
        'products-JewelryNecklacesDivasDream.json',
        'products-JewelryNecklacesSerpenti.json',
        'products-JewelryBraceletsBzero1.json',
        'products-JewelryBraceletsDivasDream.json',
        'products-JewelryBraceletsSerpenti.json',
    ]

    prod_bundler = ProductsBundler(product_files)
    prod_bundler.execute()


def populate_categories():
    """Populate categories."""
    categories = list()

    category = Category('High Jewelry', 'HighJewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Rings', 'JewelryRings', parent_id='HighJewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Jewelry', 'Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Bzero1', 'JewelryRingsBzero1', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('New York', 'JewelryRingsNewYork', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Divas Dream',
                        'JewelryDivasDream', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Serpenti',
                        'JewelryRingsSerpenti', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Bulgari', 'JewelryRingsBulgari', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Bzero1',
                        'JewelryNecklacesBzero1', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Divas Dream',
                        'JewelryNecklacesDivasDream', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Serpenti',
                        'JewelryNecklacesSerpenti', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Bzero1',
                        'JewelryBraceletsBzero1', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Divas Dream',
                        'JewelryBraceletsDivasDream', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    category = Category('Serpenti',
                        'JewelryBraceletsSerpenti', parent_id='Jewelry')
    category = dict(category)
    categories.append(category)

    write_json(categories, join(OUTPUT_BPATH, 'categories.json'))


def render_templates():
    """Render templates."""
    templater = Templater('products.xml', 'blg-products.xml')
    templater.execute()

    templater = Templater('pricebook.xml', 'blg-pricebook.xml')
    templater.execute()

    templater = Templater('inventory.xml', 'blg-inventory.xml')
    templater.execute()


def main():
    """Run the app."""
    logging.config.fileConfig('logging.ini')

    parse_products()
    join_products()
    populate_categories()
    render_templates()


if __name__ == '__main__':
    main()
