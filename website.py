"""Website Module."""
from bs4 import BeautifulSoup
import logging


class WebPage(object):
    """Webpage Class definition."""

    def __init__(self, src, category_id=None):
        """Initialize WebPage.

        Args:
            src (str): HTML page source

        """
        super(WebPage, self).__init__()
        self.logger = logging.getLogger(__name__)
        self.logger.info('Initializing WebPage')
        self.src = src
        self.set_bs_from_src()
        self.logger.info('WebPage initialized')
        self.logger.info(self)

    def set_bs_from_src(self):
        """Set BeautifulSoup object from source."""
        bs = BeautifulSoup(self.src, 'lxml')
        self.logger.info('Source parsed with BeautifulSoup')
        self.logger.debug(bs)
        self.bs = bs

    def __repr__(self):
        """Representate WebPage."""
        html_src = self.bs.prettify()
        html_src = html_src.replace('\n', '').replace('   ', '')
        return '<WebPage: {:.150}>'.format(html_src)
