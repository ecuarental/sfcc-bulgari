"""Product Module."""
import logging
from prospect import Prospect
from utils import gen_id, get_path_url
import re


class Product(object):
    """Product Class definition."""

    def __init__(self, src, category_id):
        """Initialize Product Object.

        Args:
            src (obj): BeautifulSoup object from the product item html
            category_id (str): Category ID to be assigned to each product

        Returns:
            obj: Product object

        """
        super(Product, self).__init__()
        logger_name = "{} {}".format(__name__, self.__class__.__name__)
        self.logger = logging.getLogger(logger_name)

        self.logger.info('Initializing Product')

        # Product object creation from the html source
        self.src = src
        self.set_prospect_id()
        self.category_id = category_id
        self.set_name()
        self.set_id()
        self.set_sku()
        self.set_uuid()
        self.set_desc_short()
        self.set_img_grid()
        self.set_price()

        self.logger.info('Product initialized')
        self.logger.debug(self)

    def set_prospect_id(self):
        """Set prospect id."""
        prospect_id = Prospect().id

        self.logger.info('Prospect id: %s' % prospect_id)

        self.prospect_id = prospect_id

    def set_name(self):
        """Set product name."""
        # Find name element
        name = self.src.find_all('h3')[1]
        # Transform name text to title
        name = name.text.title()
        self.name = name
        self.logger.info('Product name: %s' % self.name)

    def set_id(self):
        """Set product id."""
        self.id = gen_id(self.name)
        self.logger.info('Product id: %s' % self.id)

    def set_sku(self):
        """Set product SKU."""
        sku = self.src.find_all(attrs={'data-sku': True})[0]
        sku = sku['data-sku']
        self.logger.info('SKU: {}'.format(sku))
        self.sku = sku

    def set_uuid(self):
        """Set product uuid."""
        uuid = self.prospect_id + '-' + self.sku

        self.logger.info('Product uuid: %s' % uuid)
        self.uuid = uuid

    def set_desc_short(self):
        """Set short description."""
        desc_short = self.src.find('p', 'description').text
        self.logger.debug('Short description: {}'.format(desc_short))
        self.desc_short = desc_short

    def set_img_grid(self):
        """Set the image of the grid."""
        img = self.src.find_all('img')[0]
        img = img['src']
        img = get_path_url(img)
        self.img_grid = img

    def set_price(self):
        """Set the product price."""
        price = self.src.find_all(attrs={'data-price': True})
        self.logger.debug('Price: {}'.format(price))
        try:
            price = price[0]
            price = price['data-price']
            p = re.compile('\d+')
            price_groups = p.findall(price)
            self.logger.debug('Price regEx parsed: {}'.format(price_groups))
            price = '{},{}'.format(price_groups[0], price_groups[1])
        except Exception:
            price = '1,000'
        self.logger.debug('Price found: {}'.format(price))

        self.price = price

    def __repr__(self):
        """Representate Product."""
        return '<Product: {} - {} - {}>'\
            .format(self.category_id, self.name, self.uuid)

    def __iter__(self):
        """Return product dictionary."""
        yield ('name', self.name)
        yield ('id', self.id)
        yield ('sku', self.sku)
        yield ('uuid', self.uuid)
        yield ('descShort', self.desc_short)
        yield ('imgGrid', self.img_grid)
        yield ('categoryID', self.category_id)
        yield ('price', self.price)


class ProductGrid(object):
    """ProductGrid Class definition."""

    def __init__(self, src, category_id=None):
        """Initialize ProductGrid.

        Args:
            src (obj): BeautifulSoup object from the product grid html
            category_id (str): Category ID of the product grid

        """
        super(ProductGrid, self).__init__()
        logger_name = "{} {}".format(__name__, self.__class__.__name__)
        self.logger = logging.getLogger(logger_name)

        self.logger.info('Initialize ProductGrid')
        self.src = src

        if category_id is not None:
            self.logger.info('Set category_id from arg: {}'
                             .format(category_id))
            self.category_id = category_id
        else:
            self.set_category_id_from_bs()

        self.set_products_bs()
        self.set_products()
        self.logger.info('ProductGrid initialized')
        self.logger.info(self)

    def set_category_id_from_bs(self):
        """Set category_id from bs."""
        category_id = self.src.fing('h2').text()
        category_id = gen_id(category_id)
        self.logger.info('Set category_id from source: {}'.format(category_id))
        self.category_id = category_id

    def set_products_bs(self):
        """Set product items list."""
        products_bs = list()
        products_bs = self.src.find_all('div', 'bul-push')
        self.logger.info('{}# products found'.format(len(products_bs)))
        self.products_bs = products_bs

    def set_products(self):
        """Set product objects."""
        products = list()
        self.logger.info('Populating the product list')
        for product_bs in self.products_bs:
            product = Product(product_bs, self.category_id)
            self.logger.debug('ProductGrid append: {}'.format(product))
            products.append(product)
        self.logger.debug(products)
        self.products = products
        self.logger.info('Successfuly populated the list of {}# products '
                         .format(len(self.products)))

    def __repr__(self):
        """Representate ProductGrid object."""
        return '<Product Grid: {}# of products in {} category>'\
            .format(len(self.products), self.category_id)


class Category(object):
    """Category Class definition."""

    def __init__(self, name, name_id, parent_id='root'):
        """Initialize Category."""
        super(Category, self).__init__()
        self.name = name
        self.name_id = name_id
        self.parent_id = parent_id

    def __iter__(self):
        """Iterate over the object attributes."""
        yield ('name', self.name)
        yield ('id', self.name_id)
        yield ('parent_id', self.parent_id)
