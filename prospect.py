"""Prospect Module."""
import logging


class Prospect(object):
    """Prospect Class definition."""

    def __init__(self):
        """Initialize Prospect Class."""
        super(Prospect, self).__init__()
        self.logger = logging.getLogger(__name__)

        self.logger.info('Initializing Prospect')
        self.name = 'Bulgari'
        self.id = 'blg'
        self.logger.info('Prospect initialized')
